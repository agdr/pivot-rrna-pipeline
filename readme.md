# pivot rrna htps pipeline

1. map reads against target database (organism of interest plus plant
   chloroplast and mitochondrial rRNA).
2. map unmapped reads from #1 against microbiome database (arb-silva,
   greengenes, etc).
3. map unmapped reads from #2 against microbiome database at low stringency to
   get all rRNA reads.
4. generate OTU table and summary report.

---
